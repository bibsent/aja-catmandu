FROM maven:3-jdk-11 AS qa-builder

WORKDIR /build

COPY metadata-qa/metadata-qa-marc/ /build/
RUN mvn clean install


FROM perl:5.34

WORKDIR /app

SHELL ["/bin/bash", "-o", "pipefail", "-c"]

RUN apt-get update && apt-get install --no-install-recommends -y \
        cpanminus \
        build-essential \
        libexpat1-dev \
        libssl-dev \
        libxml2-dev \
        libxslt1-dev \
        libgdbm-dev \
        libmodule-install-perl \
        libxml-libxslt-perl \
        # curl-config trengs av Net::Curl::Easy < Search::Elasticsearch::Client::6_0
        libcurl4-openssl-dev \
        curl \
        jq \
        python3-pip \
        default-jre \
    && rm -rf /var/lib/apt/lists/* \
    && curl -L http://cpanmin.us | perl - App::cpanminus \
    \
    # Perl packages with known test failures (that we can ignore)
    && cpanm --notest MARC::File::XML \
    \
    # Other packages
    && cpanm \
        Catmandu \
        Catmandu::Fix::Date \
        Catmandu::MARC \
        Catmandu::Store::ElasticSearch \
        Catmandu::XML \
        Catmandu::Identifier \
        Search::Elasticsearch::Client::6_0 \
        Dancer \
        Dancer::Logger::Log4perl \
        Dancer::Plugin::Catmandu::OAI \
        Dancer::Plugin::Catmandu::SRU \
        Log::Dispatch::FileRotate \
        Plack::App::Proxy \
        Plack::Middleware::BotDetector \
        Plack::Middleware::AccessLog \
        Plack::Middleware::Conditional \
        Plack::Middleware::Deflater \
        Plack::Middleware::CrossOrigin \
        Perl::Critic \
        Email::Sender::Simple \
        Email::MIME::CreateHTML \
        Authen::SASL \
        MIME::Base64 \
        Time::Piece::ISO \
        Sentry::Raven \
        Math::Round \
        Log::Any::Adapter::JSON \
        Starman \
        IPC::Run \
    \
    && pip3 install --no-cache-dir \
        httpie \
        yq

COPY --from=qa-builder /build/target/metadata-qa-marc-0.5-SNAPSHOT-jar-with-dependencies.jar /app/metadata-qa-marc.jar

COPY ./app /app/app
COPY ./scripts /app/scripts
COPY ./schema /app/schema

RUN mkdir /data && chown www-data:www-data /data

# Temporarily, while waiting for PRs
COPY ./vendor /app/vendor

CMD [ "echo", "No command specified" ]

#C# MD carton exec starman --port 8080 bin/app.psgi
# Note: Must be called with init: true
# https://github.com/docker-library/docs/blob/master/perl/README.md#signal-handling-behavior-notice
