#!/usr/bin/env perl

# Ref:
# https://metacpan.org/pod/distribution/Dancer/lib/Dancer/Cookbook.pod

use strict;

# Note to self: order of imports important: Dancer first
use Dancer;
use App::Sru;
use App::Common;
use Plack::Builder;
use Log::Log4perl qw(get_logger);
use Sentry::Raven;

my $app = sub {
    my $env     = shift;
    my $request = Dancer::Request->new(env => $env);

    if (defined $ENV{'SENTRY_DSN'}) {
        Sentry::Raven->new(sentry_dsn => $ENV{'SENTRY_DSN'})->capture_errors(sub {
            Dancer->dance($request);
        });
    } else {
        Dancer->dance($request);
    }
};


print "Plack env: $ENV{PLACK_ENV}\n";
my $logger = get_logger('sruAccess');

builder {
    enable 'Deflater';

    # http://modernperlbooks.com/mt/2012/09/excluding-bot-traffic-from-access-logs-with-plack-middleware.html
    enable 'Plack::Middleware::BotDetector',
         bot_regex => qr/Googlebot|Baiduspider|Yahoo! Slurp/;

    enable_if { ! $_[0]->{ 'BotDetector.looks-like-bot'}  } 'Plack::Middleware::AccessLog',
        format => 'combined',
        logger => sub { $logger->info(@_) };

    $app;
}
