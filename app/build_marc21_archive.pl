#!/usr/bin/env perl
# Usage: perl build_marc21_archive.pl --dest [destination dir] [src dir or pattern]

### TODO: Refactor to extract marc from Elasticsearch instead. We can query on modification date and extract the marc records
### Should be MUCH faster

use strict;
use warnings;
use utf8;
use open qw/:std :utf8/;
use Cwd;
use Cwd 'abs_path';
use Data::Dumper;
use Try::Tiny;
use Time::Piece;
use Sentry::Raven;
use File::Basename;
use XML::LibXML;
use LWP::UserAgent;
use HTTP::Request;
use JSON::MaybeXS; # qw(encode_json);
use Getopt::Long qw(GetOptions);
use Time::HiRes qw(time);
use File::Spec::Functions 'catfile';
use File::Find;
use File::stat;
use File::Copy;
use File::Glob ':bsd_glob';
use Getopt::Long;
use Math::Round qw(round);
use Cwd qw(cwd);
use Time::Piece;
use Time::Piece::ISO;
use Time::Seconds;

# Configure logging

use Log::Any '$log', category => 'process_records';

use Log::Any::Adapter ('JSON', \*STDERR, log_level => 'info'); # \*: "typeglob reference" <https://perldoc.perl.org/perlsub>
$log->context->{pid} = $$;

my $xpc = XML::LibXML::XPathContext->new();
$xpc->registerNs( 'marc', 'http://www.loc.gov/MARC21/slim' );

# Options

my $dest_dir;
my $days;
my $verbose;
GetOptions(
  'dest=s'    => \$dest_dir,
  'days=i'    => \$days,
  'verbose|v' => \$verbose,
) or die "Usage: $0 --es-index=... [--dest=...] [--days=N] [-v] [file ...]\n";

if (!defined $dest_dir) {
    $log->error("No --dest dir specified");
}

my $now = localtime;
my $date_limit = $days ? $now - ( $days * ONE_DAY ) : undef;  # Constant from Time::Seconds

$dest_dir = abs_path($dest_dir);
if ( ! -d $dest_dir ) {
    $log->error("Archive dir does not exist: $dest_dir");
    exit 1;
}

my $archive_file = $dest_dir . '/all.xml';

# -----------------------------------------------------------------------------------------------

sub locate_source_files {
    my @inputs = map { bsd_glob($_) } @ARGV;
    my @files = ();
    foreach my $file (@inputs) {
        if (-f $file) {
            push @files, $file;
        } elsif (-d $file) {
            find(
                # There is a strange scalar variable called $_ in Perl, which is the `default variable`,
                # or in other words the topic. <https://perlmaven.com/the-default-variable-of-perl>
                sub {
                    my $abs_path = abs_path($File::Find::name);
                    if (-f -r && /\.xml$/i) {
                        my $file_age = time() - stat($_)->mtime;
                        # Assume file is complete if it's not modified in the last 60 seconds
                        if ($file_age > 60) {
                            push @files, abs_path($_);
                        }
                    }
                },
                $file
            );
        }
    }
    return @files
}

sub process_promus_record_v2 {
    my $record = shift;
    my $out_fh = shift;

    my $approve_date_first = $record->findvalue('./approve_date_first');
    my $approved = ($approve_date_first eq "") ? undef : $approve_date_first;

    if (defined $date_limit) {
        if (!defined $approved) {
            return 0;
        }
        $approved =~ s/Z$//;
        my $approved_date = Time::Piece::ISO->strptime($approved);
        if ($approved_date < $date_limit) {
            return 0;
        }
    }

    my $marc_record = $xpc->findnodes('marc:record', $record)->pop();
    print $out_fh $marc_record->serialize(1) . "\n";
    return 1;
}

sub make_archive {

    my @files = locate_source_files();

    my $n_files = scalar @files;
    if ($n_files == 0) {
        exit 0;
    }

    $log->info('Extract job started', {file_count => $n_files});

    open(my $out_fh, '>:encoding(UTF-8)', $archive_file) or die("Could not open file for writing: $archive_file");

    print $out_fh "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
    print $out_fh "<collection xmlns=\"http://www.loc.gov/MARC21/slim\">\n";

    # my $doc = XML::LibXML->createDocument('1.0', 'UTF-8');
    #my $collection = $doc->createElementNS('http://www.loc.gov/MARC21/slim', 'collection');
   # $doc->setDocumentElement($collection);

    my $t0 = time;
    my $n = 0;
    my $n_recs = 0;

    foreach my $file (@files) {
        $n++;
        try {
            # $log->info("Reading file");
            my $dom = XML::LibXML->load_xml(location => $file);

            if ($n % 100 == 0) {
                print("processed_files=$n total_files=$n_files matching_records=$n_recs current_file=$file\n");
            }

            # Import version 2 records, where the MARC record is encapsulated in a 'promus_record' top element
            foreach my $promus_record ($dom->findnodes('/promus_record')) {
                if (process_promus_record_v2($promus_record, $out_fh)) {
                    $n_recs++;
                }
            }

            if (!defined $date_limit) {
                # Import version 1 records, which are pure MARC
                foreach my $marc_record ($xpc->findnodes('/marc:collection/marc:record', $dom)) {
                    print $out_fh $marc_record->serialize(1) . "\n";
                    $n_recs++;
                }
            }
        }
        catch {
            $log->error('Record processing failed', { details => "$_", file => abs_path($file) });
            exit 1;
        };
    }

    print $out_fh "</collection>\n";
    close $out_fh;

    my $dt = time - $t0;
    my $speed = $n_files / $dt;
    $log->info('Job complete', { files_processed => $n_files,
        recs_per_sec                             => round($speed * 10) / 10,
        duration_secs                            => round($dt * 10) / 10 });
}

make_archive();
