#!/usr/bin/env perl
# Usage: perl import_records.pl [--flags] [file_or_folder ...]
use v5.26;  # improved unicode handling
use strict;
use warnings;
use utf8; # declares that this source unit is encoded as UTF‑8
use open qw/:std :utf8/;  # assuming that anything that opens a filehandle within this lexical scope is encoded in UTF‑8
# -
use Cwd;
use Cwd 'abs_path';
use Catmandu;
use Catmandu::Store::ElasticSearch;
use Data::Dumper;
use Encode qw(decode_utf8);
use Try::Tiny;
use Time::Piece;
use Sentry::Raven;
use File::Basename;
use XML::LibXML;
use LWP::UserAgent;
use HTTP::Request;
use JSON::MaybeXS; # qw(encode_json);
use Getopt::Long qw(GetOptions);
use Time::HiRes qw(time);
use File::Spec::Functions 'catfile';
use File::Find;
use File::stat;
use File::Copy;
use File::Glob ':bsd_glob';
use Getopt::Long;
use Math::Round qw(round);
use Cwd qw(cwd);
use Email::Sender::Simple qw(sendmail);
use Email::Sender::Transport::SMTP qw();
use Email::MIME::CreateHTML;
use Template;
use Text::CSV qw( csv );
use File::Slurp;
use IPC::Run 'run';

# Configure logging

use Log::Any '$log', category => 'import_records';

use Log::Any::Adapter ('JSON', \*STDERR, log_level => 'info'); # \*: "typeglob reference" <https://perldoc.perl.org/perlsub>
$log->context->{pid} = $$;

# Options

my $verbose;
my $dryrun_mode;
GetOptions(
  'verbose|v'        => \$verbose,
  'dry-run'          => \$dryrun_mode,
) or die "Usage: $0 --es-index=... [-v] [--dry-run] [file ...]\n";

my $enable_record_validation = 0;
if (defined $ENV{'CATMANDU_VALIDATE_MARC'} && $ENV{'CATMANDU_VALIDATE_MARC'} ne "0") {
   $enable_record_validation = 1;
}

my $enable_pimcore_push = 0;
if (defined $ENV{'CATMANDU_PIMCORE_PUSH'} && $ENV{'CATMANDU_PIMCORE_PUSH'} ne "0") {
   $enable_pimcore_push = 1;
}

my $es_index = "bibbi";
if (defined $ENV{'ES_INDEX'}) {
    $es_index = $ENV{'ES_INDEX'};
}

my $archive_dir;
if (defined $ENV{'CATMANDU_ARCHIVE_DIR'}) {
    $archive_dir = abs_path($ENV{'CATMANDU_ARCHIVE_DIR'});
    if ( ! -d $archive_dir ) {
        $log->error("Archive dir does not exist: $archive_dir");
        exit 1;
    }
}

my $marc21_output_dir;
if (defined $ENV{'CATMANDU_MARC21_EXPORT_DIR'}) {
    $marc21_output_dir = abs_path($ENV{'CATMANDU_MARC21_EXPORT_DIR'});
    if ( ! -d $marc21_output_dir ) {
        $log->error("Archive dir does not exist: $marc21_output_dir");
        exit 1;
    }
}

my $error_dir;
if (defined $ENV{'CATMANDU_ERROR_DIR'}) {
    $error_dir = abs_path($ENV{'CATMANDU_ERROR_DIR'});
    if ( ! -d $error_dir ) {
        $log->error("Error dir does not exist: $error_dir");
        exit 1;
    }
}

my $java_path = $ENV{'JAVA_PATH'} || '/usr/bin/java';
my $java_opts = $ENV{'JAVA_OPTS'} || '-Xmx4g';
my $metadata_qa_marc_path = $ENV{'METADATA_QA_MARC_PATH'} || '/app/metadata-qa-marc.jar';
my $tmp_dir = $ENV{'TMP_DIR'} || '/data/tmp';  # Must be writable
my $es_host = $ENV{'ES_HOST'} || 'localhost:9200';

my $record_url_template = $ENV{'RECORD_URL_TEMPLATE'};

$log->context->{index} = $es_index;

my @inputs = map { bsd_glob(decode_utf8($_, 1)) } @ARGV;
my @files = ();
foreach my $file (@inputs) {
    if (-f $file) {
        push @files, $file;
    } elsif (-d $file) {
        find(
            # There is a strange scalar variable called $_ in Perl, which is the `default variable`,
            # or in other words the topic. <https://perlmaven.com/the-default-variable-of-perl>
            sub {
                my $abs_path = abs_path($File::Find::name);
                if (-f -r && /\.xml$/i) {
                    my $file_age = time() - stat($_)->mtime;
                    # Assume file is complete if it's not modified in the last 60 seconds
                    if ($file_age > 60) {
                        push @files, abs_path($_);
                    }
                }
            },
            $file
        );
    }
}

my $n_files = scalar @files;
if ($n_files == 0) {
    exit 0;
}

$log->info('Import job started', {
    file_count => $n_files,
    env => $ENV{'APP_ENV'},
    metadata_qa_marc_path => $metadata_qa_marc_path}
);

# Sentry
my $sentry;
if (defined $ENV{'SENTRY_DSN'}) {
    $sentry = Sentry::Raven->new( sentry_dsn => $ENV{'SENTRY_DSN'} );
}

sub log_to_sentry {
    my $msg = shift;
    my $context = shift;
    my $level = shift;
    if ($sentry) {
        $sentry->capture_message($msg,
            extra       => $context,
            logger      => 'import_records',
            environment => $ENV{'APP_ENV'},
            level       => $level || "error"
        );
    }
}

sub log_and_die {
    my $msg = shift;
    my $context = shift;
    log_to_sentry($msg, $context);
    $log->error($msg, $context);
    exit 1;
}

if (! -d $tmp_dir) {
    mkdir($tmp_dir, 0755);
}

Catmandu->load();

my $store = Catmandu::Store::ElasticSearch->new(
    client => '6_0::Direct',
    nodes => $es_host,
    default_bag => $es_index,
    bags => {
        $es_index => {
            type => '_doc',
            on_error => 'throw'
        }
    }
);

#my $store = Catmandu->store('es_store');
my $fixer = Catmandu->fixer('marc2json.fix');

my $xpc = XML::LibXML::XPathContext->new();
$xpc->registerNs( 'marc', 'http://www.loc.gov/MARC21/slim' );


# Validate a single MARC 21 record using metadata-qa-record
sub validate_marc_record {
    my $doc = shift;
    my $marc21_file = shift;
    my $sku = shift;
    my $file_contents = read_file($marc21_file, { binmode => ':utf8' });

    run [
        $java_path,
        $java_opts,
        "-cp",
        $metadata_qa_marc_path,
        "de.gwdg.metadataqa.marc.cli.Validator",
        "--outputDir",
        $tmp_dir,
        "--format",
        "csv",
        "-d",
        "BOOKS",
        "--marcxml",
        $marc21_file
    ], ">", \my $stdout, "2>", \my $stderr or log_and_die("metadata-qa-marc returned $?");

    my $csv = Text::CSV->new();

    my @report = read_file("$tmp_dir/validation-report.txt");
    shift(@report);
    for my $line (@report) {
        # print "::: $line";

        $csv->parse($line);
        my @row = $csv->fields();

        #print Dumper(@row);

        # Accept $9 since it's a de facto standard for language codes for 6XX fields in Norway
        next if ($line =~ /"undefined subfield","9"/);

        # Accept field 019 until end of 2022
        next if ($line =~ /"undefined field","019"/);

        # Accept field 9XX until end of 2022
        next if ($line =~ /"undefined field","9/);

        my $errstring = '';
        my $record_id = $row[0];
        if ($#row == 6) {
            $errstring = "Field $row[1]: $row[4] $row[5]";
        } else {
            # Unknown format, let's just join the items
            $errstring = join ' ', @row;
        }

        $log->info('Record validation error', {error => $errstring, record_id => $record_id, path => $marc21_file});
        my $record_url = $record_url_template =~ s/{record_id}/$record_id/r;
        log_to_sentry("$errstring", {
            url => $record_url,
            path => "$marc21_file",
            error => "$errstring",
            source => $file_contents,
            sku => $sku,
        }, "warning");
    }
}

# Import a MARC record
sub import_marc_record {
    my $record = shift;
    my $dest_path = shift;
    my $sku = shift;
    my $cb = shift;
    my $doc = XML::LibXML->createDocument( '1.0', 'UTF-8' );
    my $collection = $doc->createElementNS( 'http://www.loc.gov/MARC21/slim', 'collection' );
    $doc->setDocumentElement($collection);
    $collection->appendChild($record);
    my $data = $doc->toString();

    # Store a copy of just the MARC 21 record, for further analysis
    my $marc_record_file = get_archive_path($dest_path, 1) || "$tmp_dir/tmp.xml";
    open my $out, '>', $marc_record_file or die "Failed to open file for writing: $marc_record_file - $!";
    binmode $out;
    $doc->toFH($out);
    close $out;

    if ($enable_record_validation) {
        validate_marc_record($doc, $marc_record_file, $sku);
    }

    my $importer = Catmandu->importer( 'MARC', type => 'XML', file => \$data );
    my $fixed_importer = $fixer->fix($importer);

    $fixed_importer->each(sub {
        my $item = shift;
        if ($cb) {
            $item = $cb->($item);
        }

        if ($dryrun_mode) {
            print JSON::MaybeXS->new(utf8 => 0, pretty => 1)->encode($item);
        } else {
            $store->bag->add($item);
        }
    });
}


# Import a version 2 Promus record
sub import_promus_record_v2 {
    my $record = shift;
    my $dest_path = shift;
    my $sku = shift;
    my $marc_record = $xpc->findnodes( 'marc:record', $record )->pop();
    import_marc_record($marc_record, $dest_path, $sku, sub {
        my $item = shift;
        my $deleted = $record->findvalue('./deleted');
        my $create_date = $record->findvalue('./create_date');
        my $approve_date_first = $record->findvalue('./approve_date_first');
        $item->{'created'} = ($create_date eq "") ? undef : $create_date;
        $item->{'approved'} = ($approve_date_first eq "") ? undef : $approve_date_first;
        my @sets = map { $_->to_literal(); } $record->findnodes('./sets/set');
        $item->{'set'} = \@sets;
        my $is_deleted = \0;
        if ($deleted ne "") {
            $item->{'deleted'} = \1;  # https://stackoverflow.com/a/43867848/489916
            $is_deleted = \1;
        }
        if ($verbose) {
            $log->info('Record imported', {bibbi_id => $item->{'_id'}, record_version => '2',
                                           is_deleted => $is_deleted, path => $dest_path});
        }
        push_to_pimcore($item);
        return $item;
    });
}


# Push record to Pimcore
sub push_to_pimcore {
    my $record = shift;

    if (!$enable_pimcore_push) {
        return;
    }

    my $ua = new LWP::UserAgent;
    $ua->agent("Aja-Catmandu/1.0");
    $ua->default_header('apikey' => $ENV{'AJA_PIMCORE_API_KEY'});

    if ( ! exists $record->{'isbn'}[0]) {
        $log->warn("Record doesn't have ISBN, cannot push to Pimcore");
        return;
        # Hva med ting uten IBSN?
    }
    my $isbn = $record->{'isbn'}[0];
    my $url = $ENV{'AJA_PIMCORE_URL'} . '/integrations/catmandu/products/' . $isbn;
    my $headers = ['Content-Type' => 'application/json; charset=UTF-8'];
    my $encoded_data = JSON::MaybeXS->new(utf8 => 1)->encode($record);
    my $res = $ua->request(HTTP::Request->new('PUT', $url, $headers, $encoded_data));

    if ($res->is_success) {
        $log->info('Pushed record to Pimcore', {isbn => $isbn});
        # print $res->decoded_content;
    } else {
        $log->error('Push to Pimcore failed', {isbn => $isbn,
                                               response => $res->status_line . " : " . $res->decoded_content});
        exit 1;
    }
    # print Dumper($res);
}


sub get_archive_path {
    my $file = shift;
    my $use_marc21_output = shift;

    my $base_dir = $archive_dir;
    if ($use_marc21_output) {
        $base_dir = $marc21_output_dir;
    }

    if (!$base_dir) {
        return 0;
    }

    # Vi bruker (opp til) de 3 siste tegnene i varenummer som arkivmappe.
    # Det finnes noen få produker med varenummer bestående av kun to tegn,
    # og 1 er ikke umulig, derav spørsmålstegnene i (.?.?.)
    (my $archive_bin) = $file =~ /(.?.?.)\.xml$/;

    # print("Test: ære\n");
    # print("Archive bin: $archive_bin\n");


    if (!$archive_bin) {
        $log->error('Could not determine archive path', {'file' => $file});
        exit 1;
    }

    my $archive_bin_path = $base_dir . '/' . $archive_bin;
    mkdir $archive_bin_path;
    return abs_path($archive_bin_path . '/' . basename($file));
}

my $t0 = time;
my $n = 0;

try {
    foreach my $file (@files) {
        $n++;
        my $basename = basename($file);
        (my $sku = $basename) =~ s/\.[^.]+$//;
        try {
            # $log->info("Reading file");
            my $n_recs = 0;
            my $dom = XML::LibXML->load_xml( location => $file );
            my $dest_path = get_archive_path($file, 0) || abs_path($file);

            # Import version 2 records, where the MARC record is encapsulated in a 'promus_record' top element
            foreach my $promus_record ( $dom->findnodes('/promus_record') ) {
                import_promus_record_v2($promus_record, $dest_path, $sku);
                $n_recs++;
            }

            # Import version 1 records, which are pure MARC
            foreach my $marc_record ( $xpc->findnodes('/marc:collection/marc:record', $dom) ) {
                import_marc_record($marc_record, $dest_path, $sku, sub {
                    my $item = shift;
                    if ($verbose) {
                        $log->info('Record imported', {bibbi_id => $item->{'_id'}, record_version => '1',
                                                      is_deleted => \0, path => $dest_path});
                    }
                    push_to_pimcore($item);
                    return $item;
                });
                $n_recs++;
            }

            if ($n_recs == 0) {
                $log->warn('Found no records in file', {file => $file}); # Incomplete file?

            } elsif ($archive_dir) {
                move($file, $dest_path);
            }

        } catch {
            my $args = join(', ', @ARGV);
            my $dest_path = abs_path($file);
            if ($error_dir) {
                $dest_path = catfile($error_dir, basename($file));
                move($file, $dest_path);
            }
            # Sentry accepts up to 8kB of context, so it should be ok to send the whole MARC record
            # <https://develop.sentry.dev/sdk/data-handling/>
            my $file_contents = read_file($dest_path, { binmode => ':utf8' });
            log_and_die("Record import failed: $_", {
                path => "$dest_path",
                error => "$_",
                source => $file_contents,
                sku => $sku,
            });
        };

        if ($n % 100 == 0) {
            if (! $dryrun_mode) {
                $store->bag->commit;  # If this fails, it's harder to debug which record caused the failure...
            }
            my $dt = time - $t0;
            my $speed = $n / $dt;
            $log->info('Batch complete', {files_processed => $n, files_left => $n_files - $n,
                                          recs_per_sec => round($speed*10)/10,
                                          duration_secs => round($dt*10)/10});
        }
    }

    if (! $dryrun_mode) {
        $store->bag->commit;
    }

} catch {
    print "ERR: $_";
    my $args = join(', ', @ARGV);
    $log->error('Import job failed', {details => "$_", args => "$args"});

    log_and_die("Import job failed", {
        job_argument => "$args",
        details      => "$_",
    });
};

my $dt = time - $t0;
my $speed = $n_files / $dt;
$log->info('Import job complete', {files_processed => $n_files,
                                   recs_per_sec => round($speed*10)/10,
                                   duration_secs => round($dt*10)/10});
