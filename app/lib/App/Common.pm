package App::Common;
use Dancer ':syntax';
use Plack::Builder;
use Log::Log4perl qw(get_logger);


get '/health' => sub {
    status 200 ;
    return "Perfeakta!\n" ;
};

any qr{.*} => sub {
    status 'not_found' ;
    template '404.tt' ;
};

true;
