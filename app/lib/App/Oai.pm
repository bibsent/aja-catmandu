package App::Oai;
use Dancer ':syntax';
use Catmandu;
use Dancer::Plugin::Catmandu::OAI;

Catmandu->load;

get '/' => sub {
    template 'oai/index.tt' ;
};

my $store = Catmandu->store('es_store', nodes => [$ENV{'ES_HOST'} || 'localhost:9200']);

my $mlnb_options = {
  deleted        => sub {
    my $item = shift;
    $item->{deleted};
  },

  # https://github.com/LibreCat/Dancer-Plugin-Catmandu-OAI/pull/24
  cql_filter     => 'bs.set="mlnb"',
  uri_base       => 'https://oai.aja.bs.no/mlnb',
  repositoryName => 'Poster til Nasjonalbibliotekets metadataleveranse',
  adminEmail     => 'kundeservice@bibsent.no',
  store          => $store,
};
oai_provider '/mlnb', %$mlnb_options;

my $bibbi_options = {
  deleted        => sub {
    my $item = shift;
    $item->{deleted};
  },

  # https://github.com/LibreCat/Dancer-Plugin-Catmandu-OAI/pull/24
  cql_filter     => 'bs.set="bibbi"',
  uri_base       => 'https://oai.aja.bs.no/bibbi',
  repositoryName => 'Bibbi-katalogen',
  adminEmail     => 'kundeservice@bibsent.no',
  store          => $store,
};
oai_provider '/bibbi', %$bibbi_options;

true;
