package App::Rest;
use Dancer ':syntax';
use Catmandu;
use Search::Elasticsearch;

set serializer => 'JSON';  # Dancer config option

Catmandu->load;

my $setting = {
  store         => 'es_store',
  es_host       => $ENV{'ES_HOST'} || 'localhost:9200',
  bag           => 'bibbi',
  fix           => 'rest.fix',
  default_limit => 10,
  maximum_limit => 500,
};

my $fixer = Catmandu->fixer($setting->{fix});

my $bag = Catmandu->store(
  $setting->{store},
  nodes => $setting->{es_host}
)->bag($setting->{bag});

sub search {
  my $params = shift;
  my $cql = $params->{query};
  my $default_limit = $setting->{limit} // $bag->default_limit;
  my $maximum_limit = $setting->{maximum_limit} // $bag->maximum_limit;
  my $first = int($params->{start} // 1);
  my $limit = int($params->{limit} // $default_limit);
  if ($limit < 1) {
    $limit = $default_limit;
  }
  if ($limit > $maximum_limit) {
    $limit = $maximum_limit;
  }

  return $bag->search(
    cql_query    => $cql,
    sru_sortkeys => $params->{sort},
    limit        => $limit,
    start        => $first - 1,
  );
}

sub fix_record {
  my $record = shift;
  return $fixer->fix($record)->{display};
}

get '/' => sub {
  template 'rest/index.tt' ;
};

get '/publications/v1/' => sub {
  my $params = params('query');
  my $hits = search($params);
  my $result = {
    total => $hits->total,
    records => [],
  };
  $hits->each(sub {
    push(@{$result->{records}}, fix_record($_[0]));
  });
  content_type 'application/json';
  return $result;
};

# get '/publications/v1/openapi.yaml' => sub {
#   send_file('openapi/publications/v1/openapi.yaml');
# };

get '/publications/v1/:id' => sub {
  my $recordId = params->{id};
  my $params = {query => 'dc.identifier="' . $recordId . '"'};
  my $hits = search($params);
  my $rec = $hits->next();

  content_type 'application/json';

  if (!defined $rec) {
    status 404;
    return { error => "not_found" };
  }

  return fix_record($rec);
};

true;
