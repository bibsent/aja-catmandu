package App::Sru;
use Dancer ':syntax';
use Catmandu;
use Dancer::Plugin::Catmandu::SRU;
use Data::Dumper;

Catmandu->load;
# print Dumper(Catmandu->config);

get '/' => sub {
    template 'sru/index.tt' ;
};

my $store = Catmandu->store('es_store', nodes => [$ENV{'ES_HOST'} || 'localhost:9200']);

my $mlnb_options = {
  title      => 'Biblioteksentralens metadataleveranse for NB (delsett av Bibbi-katalogen)',
  cql_filter => 'bs.set="mlnb"',
  store      => $store,
};
sru_provider '/mlnb', %$mlnb_options;

my $bibbi_options = {
  title      => 'Bibbi-katalogen',
  cql_filter => 'bs.set="bibbi"',
  store      => $store,
};
sru_provider '/bibbi', %$bibbi_options;

true;
