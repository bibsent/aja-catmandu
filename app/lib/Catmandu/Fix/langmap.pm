package Catmandu::Fix::langmap;

use Catmandu::Sane;
use Moo;
use Catmandu::Util::Path qw(as_path);
use Catmandu::Util qw(is_hash_ref is_array_ref);
use Catmandu::Fix::Has;
# use Data::Dumper;
# use Log::Any qw($log);
# use Clone qw(clone);

has path  => (fix_arg => 1); # required parameter 1
has labelKey  => (fix_arg => 1); # required parameter 2

with 'Catmandu::Fix::Builder';

sub _build_fixer {
  my ($self) = @_;
  as_path($self->path)
    ->updater(if_array_ref => sub {
      my $tmp = {};
      my $match = $_[0];
      for my $aut ( @$match ) {
        if (is_hash_ref($aut)) {
          my $id = $aut->{id};
          my $lang = $aut->{lang};
          if ($id && $lang) {
            for my $key ( keys %$aut ) {
              if ($key eq $self->labelKey) {
                $tmp->{$id}->{$self->labelKey}->{$lang} = $aut->{$self->labelKey};
              } elsif ($key ne 'lang') {
                $tmp->{$id}->{$key} = $aut->{$key};
              }
            }
          }
        }
      }
      my $out = [];
      for my $id ( keys %$tmp ) {
        push(@$out, $tmp->{$id});
      }
      # $log->info(Dumper($out));
      $out;
    });
}

1;
