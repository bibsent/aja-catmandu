use Log::Log4perl qw< :easy >;
setting log4perl => {
    tiny => 0,
    config => '
        log4perl.logger                      = DEBUG, OnFile, OnScreen
        log4perl.appender.OnFile             = Log::Log4perl::Appender::File
        log4perl.appender.OnFile.filename    = /var/log/aja-catmandu/oai_debug.log
        log4perl.appender.OnFile.mode        = append
        log4perl.appender.OnFile.layout      = Log::Log4perl::Layout::PatternLayout
        log4perl.appender.OnFile.layout.ConversionPattern = [%d] [%5p] %m%n
        log4perl.appender.OnScreen           = Log::Log4perl::Appender::ScreenColoredLevels
        log4perl.appender.OnScreen.color.ERROR = bold red
        log4perl.appender.OnScreen.color.FATAL = bold red
        log4perl.appender.OnScreen.color.OFF   = bold green
        log4perl.appender.OnScreen.Threshold = ERROR
        log4perl.appender.OnScreen.layout    = Log::Log4perl::Layout::PatternLayout
        log4perl.appender.OnScreen.layout.ConversionPattern = [%d] >>> %m%n
    ',
};
setting logger => 'log4perl';
