<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:slim="http://www.loc.gov/MARC21/slim"
                xmlns:marcxchange="info:lc/xmlns/marcxchange-v1">

  <xsl:output method="xml" indent="yes" omit-xml-declaration="yes" />

  <!-- Copy everything into the output -->
  <xsl:template match='@*|node()'>
    <xsl:copy>
      <xsl:apply-templates select='@* | node()'/>
    </xsl:copy>
  </xsl:template>

  <!-- Modify <record> elements -->
  <xsl:template match="marcxchange:record">
    <xsl:element name="record" namespace="http://www.loc.gov/MARC21/slim">
      <xsl:apply-templates select="@* | node()"/>
    </xsl:element>
  </xsl:template>

  <!--
    Change namespace of all elements from "info:lc/xmlns/marcxchange-v1" to "http://www.loc.gov/MARC21/slim"
    Source: https://stackoverflow.com/questions/42286987/xslt-to-change-namespace-in-element
  -->
  <xsl:template match="marcxchange:*">
    <xsl:element name="{local-name()}" namespace="http://www.loc.gov/MARC21/slim">
      <xsl:apply-templates select="@* | node()"/>
    </xsl:element>
  </xsl:template>

</xsl:stylesheet>
