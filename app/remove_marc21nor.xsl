<?xml version="1.0"?>
<!--
  XSLT-transformasjon som gjør om "marc21nor" (MARC 21 med norske tillegg som systemleverandørene trenger en stund til)
  til "marc21" (Ren MARC 21)
-->
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:slim="http://www.loc.gov/MARC21/slim"
                xmlns:marc="info:lc/xmlns/marcxchange-v1">

  <xsl:output method="xml" indent="yes" omit-xml-declaration="yes" />

  <!-- Identity Trasnform: Copy everything into the output -->
  <xsl:template match='@*|node()'>
    <xsl:copy>
      <xsl:apply-templates select='@* | node()'/>
    </xsl:copy>
  </xsl:template>

  <!-- except 019 and 9XX -->
  <xsl:template match="marc:datafield[@tag='019' or starts-with(@tag, '9')]"/>

</xsl:stylesheet>
