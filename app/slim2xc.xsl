<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:slim="http://www.loc.gov/MARC21/slim"
                xmlns:marcxchange="info:lc/xmlns/marcxchange-v1">

  <xsl:output method="xml" indent="yes" omit-xml-declaration="yes" />

  <!-- copy everything into the output -->
  <xsl:template match='@*|node()'>
    <xsl:copy>
      <xsl:apply-templates select='@* | node()'/>
    </xsl:copy>
  </xsl:template>

  <!-- template to modify slim:record -->
  <xsl:template match="slim:record">
    <xsl:element name="record" namespace="info:lc/xmlns/marcxchange-v1">
      <xsl:attribute name="format">MARC 21</xsl:attribute>
      <xsl:attribute name="type">Bibliographic</xsl:attribute>
      <xsl:apply-templates select="@* | node()"/>
    </xsl:element>
  </xsl:template>

  <!-- template to change the namespace
        of the elements
        from "http://www.loc.gov/MARC21/slim"
        to "info:lc/xmlns/marcxchange-v1"
      Source: https://stackoverflow.com/questions/42286987/xslt-to-change-namespace-in-element
    -->
  <xsl:template match="slim:*">
    <xsl:element name="{local-name()}" namespace="info:lc/xmlns/marcxchange-v1">
      <xsl:apply-templates select="@* | node()"/>
    </xsl:element>
  </xsl:template>

</xsl:stylesheet>
