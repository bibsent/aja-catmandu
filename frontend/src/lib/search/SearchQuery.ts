import tokenizer from 'search-text-tokenizer';
import type { QueryClause } from "$lib/types";

const componentNames = {
  genre: 'Genre',
  level: 'Level',
};

export class SearchQuery {
  clauses: QueryClause[] = [];
  sort: string;
  limit: number;

  constructor({clauses, limit, sort}) {
    this.clauses = clauses;
    this.limit = limit;
    this.sort = sort;
  }

  static getQueryClauses(params: URLSearchParams): QueryClause[] {
    const clauses = tokenizer(params.get('q') || '');
    if (!clauses.length) {
      clauses.push({tag: 'level', term: 'full'});
    }
    return clauses
      .map((clause: QueryClause) => ({
        componentName: componentNames[clause.tag],
        toString: function () {
          // POC, must be refactored
          if (this.tag == 'level') {
            return 'bs.level=full';
          }
          return `bs.authority=${this.term}`;
        },
        ...clause
      }))
      .filter((clause: QueryClause) => clause.componentName);
  }

  static async fromParams(params: URLSearchParams) {
    return new SearchQuery({
      clauses: this.getQueryClauses(params),
      limit: params.get('limit') || 25,
      sort: params.get('sort') || 'bs.approved,,0',
    });
  }

  toString() {
    if (!this.clauses.length) {
      return '';
    }
    return this.clauses
      .map(clause => clause.toString())
      .join(' AND ');
  }
}
