import { dictionary, locale, _ } from 'svelte-i18n';

function setupI18n({ withLocale: _locale } = { withLocale: 'nb' }) {
    dictionary.set({
        nb: {
            format: {
                Book: 'Bok',
                EBook: 'E-bok',
                VideoGame: 'Dataspill',
                Video: 'Film',
                AudiobookFormat: 'Lydbok',
                Hardcover: 'Innbundet bok',
            },
        },
    });
    locale.set(_locale);
}

export { _, setupI18n };
