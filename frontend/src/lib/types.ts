export interface Concept {
  id: string;
  url: string;
  name: {nob: string, nno: string};
  vocabulary: string;
}

export interface ImageObject {
  url: string | null;
  thumbnailUrl: string | null;
}

export interface Publication {
  '@id': string;
  '@type': string;
  bookFormat: string | null;
  identifier: string;
  gtin13: string | null;
  title: string | null;
  image: ImageObject;
  webdewey: [string];
  abstract: string | null;
  publisher: string | null;
  placeOfPublication: string | null;
  datePublished: string | null;
  genre: [Concept];
  subject: [Concept];
}

export interface QueryClause {
  term: string;
  tag?: string;
  componentName?: string;
  toString: () => string;
  description: string;
}
