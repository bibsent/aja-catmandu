/** Dispatch event on click outside of node */
export function clickOutside(node: Element) {

  function handleClick(this: Element, event: Event) {
    if (node && event.target instanceof Element && !node.contains(event.target) && !event.defaultPrevented) {
      node.dispatchEvent(
        new CustomEvent('click-outside')
      );
    }
  }

	document.addEventListener("click", handleClick, true);

  return {
    destroy() {
      document.removeEventListener('click', handleClick, true);
    }
	};
}
