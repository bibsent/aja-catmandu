import trimEnd from 'lodash-es/trimEnd.js'; // workaround <https://github.com/sveltejs/kit/issues/1549#issuecomment-849736405>

export const apiEndpoint = trimEnd(
  import.meta.env['VITE_CATMANDU_REST_API_URL'] || 'https://bibliografisk.bs.no/',
  '/'
);
