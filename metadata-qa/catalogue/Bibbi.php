<?php


class Bibbi extends Catalogue {

  protected $name = 'bibbi';
  protected $label = 'Bibbi-katalogen (fullstendig)';
  protected $url = 'https://bibsent.no';

  function getOpacLink($id, $record) {
    # return 'https://pid.uba.uva.nl/ark:/88238/b1' . trim($id);
    return 'https://bibliografisk.bs.no/bibbi/' . trim($id);
  }
}
