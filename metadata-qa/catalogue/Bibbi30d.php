<?php


class Bibbi30d extends Catalogue {

  protected $name = 'bibbi-30d';
  protected $label = 'Bibbi-katalogen (siste 30 dager)';
  protected $url = 'https://bibsent.no';

  function getOpacLink($id, $record) {
    # return 'https://pid.uba.uva.nl/ark:/88238/b1' . trim($id);
    return 'https://bibliografisk.bs.no/bibbi/' . trim($id);
  }
}
