#!/usr/bin/env bash
# shellcheck disable=SC2086
# Modified version of common-script
# Usage: CATALOGUE=bibbi ./script.sh all

set -eu

if [[ -z "$CATALOGUE" ]]; then
  echo "ERROR: CATALOGUE must be set"
  die 1
fi

FILE_MASK=${FILE_MASK:-*.xml}
TYPE_PARAMS=${TYPE_PARAMS:---marcxml}
DATA_DIR=${DATA_DIR:-/data}

INPUT_DIR=${INPUT_DIR:-$DATA_DIR/input/$CATALOGUE}
OUTPUT_DIR=${OUTPUT_DIR:-$DATA_DIR/output/$CATALOGUE}
LOG_DIR=${LOG_DIR:-$DATA_DIR/logs/$CATALOGUE}

SECONDS=0

echo "-------------------------------------------------------------------"
echo "INPUT_DIR: ${INPUT_DIR}"
echo "OUTPUT_DIR: ${OUTPUT_DIR}"
echo "LOG_DIR: ${LOG_DIR}"
echo "-------------------------------------------------------------------"

mkdir -p "$LOG_DIR"

# printf "%s> Logging to ${LOG_DIR}.log"

if [ ! -d "${OUTPUT_DIR}" ]; then
  mkdir -p "${OUTPUT_DIR}"
fi

do_validate() {
  GENERAL_PARAMS="--details --trimId --summary --format csv --defaultRecordType BOOKS"
  OUTPUT_PARAMS="--outputDir ${OUTPUT_DIR} --detailsFileName issue-details.csv --summaryFileName issue-summary.csv"

  printf "%s> [validator]\n" "$(date +"%F %T")"
  printf "%s> ./validator -Xms8g ${GENERAL_PARAMS} ${OUTPUT_PARAMS} ${TYPE_PARAMS} "${INPUT_DIR}/${FILE_MASK}" 2> ${LOG_DIR}/validate.log\n" "$(date +"%F %T")"
  ./validator ${GENERAL_PARAMS} ${OUTPUT_PARAMS} ${TYPE_PARAMS} ${INPUT_DIR}/$FILE_MASK 2> ${LOG_DIR}/validate.log
}

do_prepare_solr() {
  printf "%s> [prepare-solr]\n" "$(date +"%F %T")"
  printf "%s> ./prepare-solr ${CATALOGUE} 2> ${LOG_DIR}/solr.log\n" "$(date +"%F %T")"
  ./prepare-solr $CATALOGUE 2> ${LOG_DIR}/solr.log
}

do_index() {
  PARAMS=$(echo ${TYPE_PARAMS} | sed -r 's/--emptyLargeCollectors//')
  printf "%s> [index]\n" "$(date +"%F %T")"
  printf "%s> ./index --db $CATALOGUE ${PARAMS} --trimId "${INPUT_DIR}/${FILE_MASK}" 2> ${LOG_DIR}/solr.log\n" "$(date +"%F %T")"
  ./index --db "$CATALOGUE" ${PARAMS} --trimId "${INPUT_DIR}/${FILE_MASK}" 2>> ${LOG_DIR}/solr.log
}

do_completeness() {
  PARAMS=$(echo ${TYPE_PARAMS} | sed -r 's/--emptyLargeCollectors|--with-delete//')
  printf "%s> [completeness]\n" "$(date +"%F %T")"
  printf "%s> ./completeness --defaultRecordType BOOKS ${PARAMS} --outputDir ${OUTPUT_DIR}/ "${INPUT_DIR}/${FILE_MASK}" 2> ${LOG_DIR}/completeness.log\n" "$(date +"%F %T")"
  ./completeness --defaultRecordType BOOKS ${PARAMS} --outputDir ${OUTPUT_DIR}/ "${INPUT_DIR}/${FILE_MASK}" 2> ${LOG_DIR}/completeness.log
}

do_classifications() {
  PARAMS=$(echo ${TYPE_PARAMS} | sed -r 's/--emptyLargeCollectors|--with-delete//')
  printf "%s> [classifications]\n" "$(date +"%F %T")"
  printf "%s> ./classifications --defaultRecordType BOOKS ${PARAMS} --outputDir ${OUTPUT_DIR}/ "${INPUT_DIR}/${FILE_MASK}" 2> ${LOG_DIR}/classifications.log\n" "$(date +"%F %T")"
  ./classifications --defaultRecordType BOOKS ${PARAMS} --outputDir ${OUTPUT_DIR}/ "${INPUT_DIR}/${FILE_MASK}" 2> ${LOG_DIR}/classifications.log
  printf "%s> Rscript scripts/classifications-type.R ${OUTPUT_DIR} 2>> ${LOG_DIR}/classifications.log\n" "$(date +"%F %T")"
  Rscript scripts/classifications-type.R ${OUTPUT_DIR}
}

do_authorities() {
  PARAMS=$(echo ${TYPE_PARAMS} | sed -r 's/--emptyLargeCollectors|--with-delete//')
  printf "%s> [authorities]\n" "$(date +"%F %T")"
  printf "%s> ./authorities --defaultRecordType BOOKS ${PARAMS} --outputDir ${OUTPUT_DIR}/ "${INPUT_DIR}/${FILE_MASK}" 2> ${LOG_DIR}/authorities.log\n" "$(date +"%F %T")"
  ./authorities --defaultRecordType BOOKS ${PARAMS} --outputDir ${OUTPUT_DIR}/ "${INPUT_DIR}/${FILE_MASK}" 2> ${LOG_DIR}/authorities.log
}

do_tt_completeness() {
  PARAMS=$(echo ${TYPE_PARAMS} | sed -r 's/--emptyLargeCollectors|--with-delete//')
  printf "%s> [tt-completeness]\n" "$(date +"%F %T")"
  printf "%s> ./tt-completeness --defaultRecordType BOOKS ${PARAMS} --outputDir ${OUTPUT_DIR}/ --trimId "${INPUT_DIR}/${FILE_MASK}" 2> ${LOG_DIR}/tt-completeness.log\n" "$(date +"%F %T")"
  ./tt-completeness --defaultRecordType BOOKS ${PARAMS} --outputDir ${OUTPUT_DIR}/ --trimId "${INPUT_DIR}/${FILE_MASK}" 2> ${LOG_DIR}/tt-completeness.log

  printf "%s> Rscript scripts/tt-histogram.R ${OUTPUT_DIR} &>> ${LOG_DIR}/tt-completeness.log\n" "$(date +"%F %T")"
  Rscript scripts/tt-histogram.R ${OUTPUT_DIR} &>> ${LOG_DIR}/tt-completeness.log
}

do_shelf_ready_completeness() {
  PARAMS=$(echo ${TYPE_PARAMS} | sed -r 's/--emptyLargeCollectors|--with-delete//')
  printf "%s> [shelf-ready-completeness]\n" "$(date +"%F %T")"
  printf "%s> ./shelf-ready-completeness --defaultRecordType BOOKS ${PARAMS} --outputDir ${OUTPUT_DIR}/ --trimId "${INPUT_DIR}/${FILE_MASK}" 2> ${LOG_DIR}/shelf-ready-completeness.log\n" "$(date +"%F %T")"
  ./shelf-ready-completeness \
    --defaultRecordType BOOKS \
    ${PARAMS} \
    --outputDir ${OUTPUT_DIR}/ \
    --trimId ${INPUT_DIR}/${FILE_MASK} 2> ${LOG_DIR}/shelf-ready-completeness.log

  printf "%s> Rscript scripts/shelf-ready-histogram.R ${OUTPUT_DIR} &>> ${LOG_DIR}/shelf-ready-completeness.log\n" "$(date +"%F %T")"
  Rscript scripts/shelf-ready-histogram.R ${OUTPUT_DIR} &>> ${LOG_DIR}/shelf-ready-completeness.log
}

do_serial_score() {
  PARAMS=$(echo ${TYPE_PARAMS} | sed -r 's/--emptyLargeCollectors|--with-delete//')
  printf "%s> [serial-score]\n" "$(date +"%F %T")"
  printf "%s> ./serial-score --defaultRecordType BOOKS ${PARAMS} --outputDir ${OUTPUT_DIR}/ --trimId "${INPUT_DIR}/${FILE_MASK}" 2> ${LOG_DIR}/serial-score.log\n" "$(date +"%F %T")"
  ./serial-score --defaultRecordType BOOKS \
                 ${PARAMS} \
                 --outputDir ${OUTPUT_DIR}/ \
                 --trimId "${INPUT_DIR}/${FILE_MASK}" 2> ${LOG_DIR}/serial-score.log

  printf "%s> Rscript scripts/serial-score-histogram.R ${OUTPUT_DIR} &>> ${LOG_DIR}/serial-score.log\n" "$(date +"%F %T")"
  Rscript scripts/serial-score-histogram.R ${OUTPUT_DIR} &>> ${LOG_DIR}/serial-score.log
}

do_format() {
  ./formatter --defaultRecordType BOOKS "${INPUT_DIR}/${FILE_MASK}"
}

do_functional_analysis() {
  PARAMS=$(echo ${TYPE_PARAMS} | sed -r 's/--emptyLargeCollectors|--with-delete//')
  printf "%s> [functional-analysis]\n" "$(date +"%F %T")"
  printf "%s> ./functional-analysis --defaultRecordType BOOKS ${PARAMS} --outputDir ${OUTPUT_DIR}/ "${INPUT_DIR}/${FILE_MASK}" 2> ${LOG_DIR}/functional-analysis.log\n" "$(date +"%F %T")"
  ./functional-analysis --defaultRecordType BOOKS \
                        ${PARAMS} \
                        --outputDir ${OUTPUT_DIR}/ "${INPUT_DIR}/${FILE_MASK}" 2> ${LOG_DIR}/functional-analysis.log
}

do_network_analysis() {
  PARAMS=$(echo ${TYPE_PARAMS} | sed -r 's/--emptyLargeCollectors|--with-delete//')
  printf "%s> [network-analysis]\n" "$(date +"%F %T")"
  printf "%s> ./network-analysis --defaultRecordType BOOKS ${PARAMS} --outputDir ${OUTPUT_DIR}/ "${INPUT_DIR}/${FILE_MASK}" 2> ${LOG_DIR}/network-analysis.log\n" "$(date +"%F %T")"
  ./network-analysis --defaultRecordType BOOKS \
                     ${PARAMS} \
                     --outputDir ${OUTPUT_DIR}/ \
                     "${INPUT_DIR}/${FILE_MASK}" 2> ${LOG_DIR}/network-analysis.log

  # network.csv (concept, id) ->
  #   network-by-concepts.csv (concept, count, ids)
  #   network-by-record.csv (id, count, concepts)
  #   network-statistics.csv (type, total, single, multi)
  printf "%s> Rscript scripts/network-transform.R ${OUTPUT_DIR} &>> ${LOG_DIR}/network-analysis.log\n" "$(date +"%F %T")"
  Rscript scripts/network-transform.R ${OUTPUT_DIR} &>> ${LOG_DIR}/network-analysis.log

  # network-by-concepts (concept, count, ids) ->
  #   network-pairs.csv (id1 id2)
  #   network-nodes.csv (id, id)
  printf "%s> ./network-analysis --outputDir ${OUTPUT_DIR} --action pairing --group-limit 2000 &>> ${LOG_DIR}/network-analysis.log\n" "$(date +"%F %T")"
  ./network-analysis --outputDir ${OUTPUT_DIR} \
                     --action pairing \
                     &>> ${LOG_DIR}/network-analysis.log

  cat network-pairs.csv | sort | uniq -c | sort -nr > network-pairs-uniq-with-count.csv
  awk '{print $2 " " $3}' network-pairs-uniq-with-count.csv > network-pairs-all.csv

  printf "%s> ziping output\n" "$(date +"%F %T")"
  PWD=$(pwd)
  cd ${OUTPUT_DIR}
  zip network-input network-nodes.csv network-nodes-???.csv network-pairs-???.csv network-by-concepts-tags.csv
  cd $PWD



  printf "%s> upload output\n" "$(date +"%F %T")"
  scp ${OUTPUT_DIR}/network-input.zip pkiraly@roedel.etrap.eu:/roedel/pkiraly/network/input

  # spark-shell -I scripts/network.scala --conf spark.driver.metadata.qa.dir="${OUTPUT_DIR}"
  # ./network-export.sh ${OUTPUT_DIR}
}

do_pareto() {
  printf "%s> [pareto]\n" "$(date +"%F %T")"
  printf "%s> Rscript scripts/frequency-range.R ${OUTPUT_DIR} &> ${LOG_DIR}/pareto.log\n" "$(date +"%F %T")"
  Rscript scripts/frequency-range.R ${OUTPUT_DIR} &> ${LOG_DIR}/pareto.log
}

do_marc_history() {
  PARAMS=$(echo ${TYPE_PARAMS} | sed -r 's/--emptyLargeCollectors|--with-delete//')
  printf "%s> [marc-history]\n" "$(date +"%F %T")"
  printf "%s> ./formatter --selector \"008~7-10;008~0-5\" --defaultRecordType BOOKS ${PARAMS} --separator \",\" --outputDir ${OUTPUT_DIR}/ "${INPUT_DIR}/${FILE_MASK}" &> ${LOG_DIR}/marc-history.log\n" "$(date +"%F %T")"
  ./formatter --selector "008~7-10;008~0-5" --defaultRecordType BOOKS ${PARAMS} --separator "," --outputDir ${OUTPUT_DIR}/ "${INPUT_DIR}/${FILE_MASK}" &> ${LOG_DIR}/marc-history.log

  printf "%s> Rscript scripts/marc-history.R ${OUTPUT_DIR} &>> ${LOG_DIR}/marc-history.log\n" "$(date +"%F %T")"
  Rscript scripts/marc-history.R ${OUTPUT_DIR} &>> ${LOG_DIR}/marc-history.log
}

do_record_patterns() {
  printf "%s> [record-patterns]\n" "$(date +"%F %T")"
  printf "%s> Rscript scripts/top-fields.R ${OUTPUT_DIR} &>> ${LOG_DIR}/top-fields.log\n" "$(date +"%F %T")"
  Rscript scripts/top-fields.R ${OUTPUT_DIR} &>> ${LOG_DIR}/top-fields.log

  PARAMS=$(echo ${TYPE_PARAMS} | sed -r 's/--emptyLargeCollectors|--with-delete//')
  printf "%s> ./record-patterns --defaultRecordType BOOKS ${PARAMS} --outputDir ${OUTPUT_DIR}/ "${INPUT_DIR}/${FILE_MASK}" &> ${LOG_DIR}/record-patterns.log\n" "$(date +"%F %T")"
  ./record-patterns --defaultRecordType BOOKS ${PARAMS} --outputDir ${OUTPUT_DIR}/ "${INPUT_DIR}/${FILE_MASK}" &> ${LOG_DIR}/record-patterns.log

  head -1 ${OUTPUT_DIR}/record-patterns.csv | sed -e 's/^/count,/' > ${OUTPUT_DIR}/record-patterns-groupped.csv
  cat ${OUTPUT_DIR}/record-patterns.csv \
    | grep -v "\\$" \
    | sort \
    | uniq -c \
    | sort -n -r \
    | sed -r 's/^ *([0-9]+) /\1,/' >> ${OUTPUT_DIR}/record-patterns-groupped.csv
}

do_all_analyses() {
  do_validate
  do_completeness
  do_classifications
  do_authorities
  do_tt_completeness
  do_shelf_ready_completeness
  do_serial_score
  do_functional_analysis
  # do_network_analysis
  do_pareto
  do_marc_history
}

do_all_solr() {
  do_prepare_solr
  do_index
}

case "$1" in
  validate)
    do_validate
    ;;
  prepare-solr)
    do_prepare_solr
    ;;
  index)
    do_index
    ;;
  completeness)
    do_completeness
    ;;
  classifications)
    do_classifications
    ;;
  authorities)
    do_authorities
    ;;
  tt-completeness)
    do_tt_completeness
    ;;
  shelf-ready-completeness)
    do_shelf_ready_completeness
    ;;
  serial-score)
    do_serial_score
    ;;
  format)
    do_format
    ;;
  functional-analysis)
    do_functional_analysis
    ;;
  network-analysis)
    do_network_analysis
    ;;
  pareto)
    do_pareto
    ;;
  marc-history)
    do_marc_history
    ;;
  record-patterns)
    do_record_patterns
    ;;
  all-analyses)
    do_all_analyses
    ;;
  all-solr)
    do_all_solr
    ;;
  all)
    do_all_solr
    do_all_analyses
    ;;
esac

duration=$SECONDS
hours=$(($duration / (60*60)))
mins=$(($duration % (60*60) / 60))
secs=$(($duration % 60))

printf "%s> DONE. %02d:%02d:%02d elapsed.\n" "$(date +"%F %T")" $hours $mins $secs

date > "$OUTPUT_DIR/last-update.csv"
