#!/bin/bash
set -e

echo HELLO Solr

# Workaround for https://issues.apache.org/jira/browse/SOLR-7323
cp -r /opt/solr/server/solr/configsets /var/solr/data/

exec solr-foreground