#!/usr/bin/env bash

set -eu

DATA_DIR=${DATA_DIR:-}

echo "============================================================================================"
echo " :: start-server.sh"

if [[ -z "${DATA_DIR:-}" ]]; then
  echo "ERROR: Environment variable DATA_DIR must be defined"
  exit 1
fi

SMARTY_VERSION=${SMARTY_VERSION:-3.1.33}

# PHP_ENV: Set to 'production' in a production environment. Defaults to 'development'
PHP_ENV=${PHP_ENV:-development}
PHP_ERROR_REPORTING=${PHP_ERROR_REPORTING:-E_ALL \\& ~E_NOTICE}

echo " :: DATA_DIR: $DATA_DIR"
echo " :: PHP_ENV: $PHP_ENV"

cp "/usr/local/etc/php/php.ini-$PHP_ENV" /usr/local/etc/php/php.ini
echo "error_log = /dev/stderr" >> /usr/local/etc/php/php.ini

sed -i "s/error_reporting = .*/error_reporting = ${PHP_ERROR_REPORTING}/" /usr/local/etc/php/php.ini

cd /var/www/html/metadata-qa-marc-web

if [[ ! -d ./libs ]]; then
  echo "Smarty is missing, will download"
  mkdir ./libs
  cd ./libs
  curl -s -L "https://github.com/smarty-php/smarty/archive/v${SMARTY_VERSION}.zip" --output "v$SMARTY_VERSION.zip"
  unzip -q "v${SMARTY_VERSION}.zip"
  rm "v${SMARTY_VERSION}.zip"
  cd ..
fi

mkdir -p images
mkdir -p cache

# Symlink images
if [[ -n "${CATALOGUE:-}" ]]; then
  echo "Symlinking: /var/www/html/metadata-qa-marc-web/images/${CATALOGUE} → ${DATA_DIR}/${CATALOGUE}/img"
  rm -f "/var/www/html/metadata-qa-marc-web/images/${CATALOGUE}"
  ln -s "${DATA_DIR}/${CATALOGUE}/img" "/var/www/html/metadata-qa-marc-web/images/${CATALOGUE}"
fi
if [[ -n "${CATALOGUES:-}" ]]; then
  for CAT in ${CATALOGUES}; do
    echo "Symlinking: /var/www/html/metadata-qa-marc-web/images/${CAT} → ${DATA_DIR}/${CAT}/img"
    rm -f "/var/www/html/metadata-qa-marc-web/images/${CAT}"
    ln -s "${DATA_DIR}/${CAT}/img" "/var/www/html/metadata-qa-marc-web/images/${CAT}"
  done
fi

exec apache2-foreground
