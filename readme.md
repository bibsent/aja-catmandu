## Development environment

Install [Shellcheck](https://www.shellcheck.net/) and [Lefthook](https://github.com/evilmartians/lefthook/blob/master/docs/full_guide.md), then run

    lefthook install

in the project folder to enable Lefthook for this project.

Configuration: Create an [.env file](https://metacpan.org/pod/Dotenv) from the distributed version:

    cp .env.dist .env

and optionally adjust the configuration:

- `SENTRY_DSN`: DSN for error reporting to [Sentry](https://sentry.io/): Leave blank in development environment
- `AJA_PIMCORE_*`: URL and API key used to call to notify Pimcore about new records. Leave blank in development environment unless you actually want to test this.
- `MAIL_*` and `SMTP_*`: Settings used for sending validation error e-mails. Can be left blank if you don't want to test this.
- `METADATA_QA_MARC_PATH`: Path to [metadata-qa-marc](https://github.com/pkiraly/metadata-qa-marc)
- `JAVA_PATH`: Path to the Java version to be used to run metadata-qa-marc.
- `RECORD_URL_TEMPLATE`: Used in the validation error e-mails to link to a record by ID.
- `TMP_DIR`: A writable directory that we can store files when running metadata-qa-marc.


Build and start services (takes a while):

    docker-compose up --build -d

Once at least the Elasticsearch container is healthy,
we can import some sample Promus records from the `./data/promus` folder:

    docker-compose run import-data

To test a single record, pass the path as an argument:

    docker-compose run import-data /data/promus/9788274889019.xml

The import-data command will extract MARC 21 records from Promus records and store them in `/data/qa/input/bibbi`,
for (optional) processing with QA Catalogue:

    CATALOGUE=bibbi docker-compose run qa

The following services should now be available:

* SRU: http://localhost:8001/mlnb?version=1.1&operation=searchRetrieve&maximumRecords=100&recordSchema=marc21&query=bs.set=mlnb
* OAI-PMH: http://localhost:8002/mlnb?verb=ListRecords&metadataPrefix=marc21
* JSON-API: http://localhost:8003/publications/v1/
* Demo-frontend for JSON-API: http://localhost:8004/
* QA catalogue: http://localhost:8005/
* Backend services:
  * Elasticsearch for Catmandu: http://localhost:9200/
  * Solr for QA catalogue: http://localhost:8983/


## Notes on Catmandu

- In Catmandu, a *bag* is a collection of *documents*.
  A bag translates to an *index* in Elasticsearch.

- Elasticsearch used to have a concept of *document type*,
  but this has been deprecated in Elasticsearch 6.
  To prepare for the eventual removal of document types, the
  recommendation is is to always use a single document type value `"_doc"`.
  Catmandu, by default, uses the bag name both as index name and document type,
  but allows us to override this in `catmandu.yml`, so we do that:

  ```
  store:
    es_store:
      package: Elasticsearch
      options:
        default_bag: bibbi
        client: 6_0::Direct
        bags:
          bibbi:
            type: _doc
  ```

  Now, Catmandu will use the `bibbi` index, but the `_doc` type.

- To importa a single MARC XML file into a store:

  ```
  $ catmandu --debug import MARC --type XML --fix marc2json.fix to es_store < 9788205528086.xml
  ```

- We always work with aliases, not directly with indices.
  Initially, `bibbi` is an alias to `bibbi_v1`.
  When a mapping schema update is necessary, we can create, populate and test `bibbi_v2`,
  before updating the alias to point to the new version.
  This strategy allows for minimal or no downtime.

- Log files are found in `/var/log/aja-catmandu/`

### Known issues

- Indices have to lowercased, not camelCased: https://github.com/LibreCat/Catmandu-Store-Elasticsearch/issues/29

- Catmandu currently supports ElasticSearch 6, testing with ElasticSearch 7 tbd.

## Services

### OAI-PMH service

Should validate at https://validator.oaipmh.com/ , with the following exceptions:

- Content-Type: The spec requires "text/xml", but Dancer returns "application/xml", which is the most common conten-type for XML today, so it would be weird if any client actually didn't accept this.
- ListSets: We don't use Sets.
- `<dc:subject xsi:type="dcterms:DDC">` produces the error "Cannot resolve 'dcterms:DDC' to a type definition for element 'dc:subject'." I'm not quite sure why, it's used as an example at https://www.dublincore.org/specifications/dublin-core/dc-xml-guidelines/

## Elasticsearch from the command line

### Inspecting a single document

Elasticsearch respones can be made more readable by piping them to
[`yq`](https://kislyuk.github.io/yq/), which formats them as YAML.
Since `yq` is a wrapper around [`jq`](https://stedolan.github.io/jq/),
we can use the `jq` language to manipulate the result. For instance, we can remove
the `xml` field to make the output more readable:

    curl -s localhost:9200/bibbi/_search | yq -y '.hits.hits[]._source | del(.|.xml)'

Or just extract a single field like `isbn`:

    curl -s localhost:9200/bibbi/_search?size=10000\&level:full%20NOT%20_exists_:approved | yq '.hits.hits[]._source.isbn'

### Querying using YAML

When writing queries by hand, it can sometimes be more convenient to write queries use YAML as well:

    ---
    query:
      bool:
        must:
          - range:
              approved:
                gte: 2021-03-01
                lt: 2021-04-01
          - term:
              level: full
        must_not:
          match:
            search.resource_type: audiobook

We can then use `yq -c . ` to convert the YAML to JSON before piping ti to Elasticsearch like so:

    cat query4.yml | yq -c . | curl -s -d@- -H 'Content-Type: application/json' localhost:9200/bibbi/_search?size=10000

### Index management

#### Delete index:

    curl -XDELETE localhost:9200/bibbi_v1

#### Create index:

Note that ES only accepts JSON here, not YAML. So we convert using yq:

    cat create_index_bibbi.yml | yq . | curl -XPUT -H "Content-Type: application/json" --data '@-' localhost:9200/bibbi_v1

#### Alias management

List:

    curl -XGET localhost:9200/_cat/aliases?v

Add:

    curl -XPOST localhost:9200/_aliases -H "Content-Type: application/json" --data @- << EOF
    {
       "actions" : [
           { "add" : { "index" : "bibbi_v1", "alias" : "bibbi" } }
       ]
    }
    EOF

Remove:

    curl -XPOST localhost:9200/_aliases -H "Content-Type: application/json" --data @- << EOF
      {
          "actions" : [
              { "remove" : { "alias" : "bibbi" } }
          ]
      }
    EOF

Swap:

    curl -XPOST localhost:9200/_aliases -H "Content-Type: application/json" --data @- << EOF
    {
       "actions" : [
           { "remove" : { "index" : "bibbi_v1", "alias" : "bibbi" } },
           { "add" : { "index" : "bibbi_v2", "alias" : "bibbi" } }
       ]
    }

## Making changes to the MARC mapping

The MARC mapping rules are defined in `app/marc2json.fix`.
After making changes, check if you need to also update `schema/bibbi_schema.yml`
and the other `*.fix` files.

To test the updated mapping, you can either use `catmandu convert`:

    catmandu convert MARC --type XML --fix marc2json.fix < example_record.xml | jq .

or just run

    docker-compose run import-data

to re-import the sample records using the updated mapping. If the schema has changed, the easiest is
to just drop the Elasticsearch volume and re-recreate the container:

    docker-compose down
    docker container prune
    docker volume prune    # Be careful that you don't delete other volumes that you actually want to keep!

### Re-indexing

After changing the mapping rules, you should normally re-build the Elasticsearch index.
First, check if you need to make changes to the Elasticsearch schema defined in `bibbi_v1.yml`.
Then run the script

    /opt/aja-catmandu/deploy/reindex.sh

which should be run as the `deploy` user on the server(s).
The current version of the script will stop the service, re-create the index and then bulk-import all MARC XML records from the archive folder:

    /var/www/webdav/promus

While the process is quite fast, a few minutes of downtime is needed (and more if something goes wrong).
A future version of the script should create a new index, import the records into the new index,
then update an alias to point to the new index and finally remove the old index .
This will allow for re-indexing with no downtime.

To test if the whole archive can be re-indexed without errors, you can copy the the archive from prod:

    rsync --stats -r aja-catmandu-prod01:/var/www/webdav/promus/arkiv/ arkiv/

and either test it locally, or rsync it to the test server:

    rsync --rsync-path 'sudo -u www-data rsync' --stats -r arkiv/ aja-catmandu-test01:/var/www/webdav/promus/arkiv/

and run the re-indexing script there.

