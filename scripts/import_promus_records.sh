#!/bin/bash
#
# Note: This script should always be run as the "www-data" user.
# It's normally run as a systemd service (/etc/systemd/system/aja-catmandu-importer.service)
# but can be run manually.
#
# Usage: CATMANDU_VALIDATE_MARC=1 ./import_promus_records.sh {file or folder} [--pimcore-push]

# -------------------------------------------------------------------------------
set -u  # Note: We don't use set -eo pipefail here, since we don't want it to exit when import_records.pl exists with exit code !=1
IFS=$'\n\t'
shopt -s nullglob

# -------------------------------------------------------------------------------
# OPTIONS

WATCH=0
DO_MIGRATE=0
# Source: https://stackoverflow.com/a/14203146/489916
POSITIONAL=()
while [[ $# -gt 0 ]]; do
  key="$1"
  case $key in
    --migrate)
      DO_MIGRATE=1
      shift # past argument
      ;;
    --watch)
      WATCH=1
      shift # past argument
      ;;
    *)    # unknown option
      POSITIONAL+=("$1") # save it in an array for later
      shift # past argument
      ;;
  esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters

# -------------------------------------------------------------------------------
# Add exit trap (WIP)

function handle_exit() {
  echo "[FATAL] Exiting, received $1 signal on line $2";
  exit 1
}
trap_with_arg() {
  func="$1" ; shift
  for sig ; do
    # shellcheck disable=SC2064
    trap "$func $sig \$LINENO" "$sig"
  done
}
trap_with_arg handle_exit SIGHUP SIGINT SIGQUIT SIGABRT SIGTERM

# -------------------------------------------------------------------------------
# Main

# Expand paths before changing dir, so we don't break relative paths
IMPORT_PATHS=()
for arg; do
  IMPORT_PATHS+=("$(realpath "$arg")")
done

# Change work dir, so it finds .env, config files, Catmandu fixes etc.
CATMANDU_DIR="$(dirname "$(dirname "$(realpath "$0")")")"
echo "Starting Catmandu import script. Dir: $CATMANDU_DIR"
if [ ! $WATCH -eq 0 ]; then
  echo "Starting in watch mode"
fi

cd "$CATMANDU_DIR/app" || exit 1

# Run migrations first, if requested
if [[ $DO_MIGRATE -eq 1 ]]; then
  "$CATMANDU_DIR/scripts/migrate.sh"
fi

echo "Running import_records.pl"
export PERL_UNICODE=AS  # A: makes all Perl scripts decode @ARGV as UTF‑8 strings
                        # S: sets the encoding of stdin, stdout, and stderr to UTF‑8
                        # <https://stackoverflow.com/a/6163129/489916>
while true; do
  perl import_records.pl --verbose "${IMPORT_PATHS[@]}" 2>&1 | grep -v '"elasticsearch.event"'
  [ $WATCH -eq 0 ] && break;
  sleep 5
done

echo "Exiting Catmandu import script"
