#!/bin/bash
#
# Usage: ./migrate.sh

# Bash strict mode
set -euo pipefail

ES_HOST=${ES_HOST:-localhost:9200}
ES_INDEX=${ES_INDEX:-bibbi_v1}
ES_ALIAS=${ES_ALIAS:-bibbi}

info () {
  jq -cn --arg message "$*" '{"level": "info", "message": $message}'
}

error () {
  jq -cn --arg message "$*" '{"level": "error", "message": $message}'
}

CATMANDU_DIR=$(dirname "$(dirname "$(realpath "$0")")")
cd "$CATMANDU_DIR"

info "Host: $ES_HOST  Index: $ES_INDEX  Alias: $ES_ALIAS  Dir: $CATMANDU_DIR"

# Check if index exists
if http --check-status --quiet HEAD "${ES_HOST}/${ES_INDEX}" 2>/dev/null; then
  info "Index already exists: ${ES_INDEX}, no need to migrate"
else
  yq -c . ./schema/bibbi_schema.yml | http --check-status --quiet --body PUT "${ES_HOST}/${ES_INDEX}?include_type_name=false"
  info "Created index ${ES_INDEX}"
fi

if [[ "$ES_ALIAS" == "NONE" ]]; then
  exit 0;
fi

# Check if alias exists
if http --check-status --quiet HEAD "${ES_HOST}/${ES_ALIAS}" 2>/dev/null; then
  if http --check-status --quiet HEAD "${ES_HOST}/_alias/${ES_ALIAS}" 2>/dev/null; then
    info "Alias already exists: ${ES_ALIAS}, no need to create"
  else
    error "Index is not an alias: ${ES_ALIAS}, you need to delete it manually"
    echo
    echo "curl -XDELETE  $ES_HOST:9200/bibbi"
    echo
  fi
else

  http --quiet --check-status --body POST "${ES_HOST}/_aliases" << EOF
  {
      "actions" : [
          { "add" : { "index" : "${ES_INDEX}", "alias" : "${ES_ALIAS}" } }
      ]
  }
EOF
  info "Added alias ${ES_ALIAS} -> ${ES_INDEX}"
fi
