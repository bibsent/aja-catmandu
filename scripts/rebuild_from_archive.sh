#!/bin/bash

# Bash strict mode
set -euo pipefail
IFS=$'\n\t'

ES_HOST=${ES_HOST:-localhost:9200}
ES_ALIAS=${ES_ALIAS:-bibbi}
ARCHIVE_DIR=$(realpath "${1-/data/arkiv}")

info () {
  jq -cn --arg message "$*" '{"level": "info", "message": $message}'
}

die () {
  jq -cn --arg message "$*" '{"level": "error", "message": $message}'
  exit 1
}

OLD_VERSION=$(curl -sS --fail "$ES_HOST/$ES_ALIAS/_alias" | jq -r 'keys[0]' | sed -E 's/.*v([0-9]+)/\1/')
[ -z "$OLD_VERSION" ] && die "ERR: Could not find current index version"
[ "$OLD_VERSION" == "bibbi" ] && die "Elasticsearch alias does not exist. Please re-run migrate.sh"


OLD_INDEX="${ES_ALIAS}_v${OLD_VERSION}"

echo "OLD: $OLD_VERSION"

NEW_VERSION=$((OLD_VERSION + 1))
NEW_INDEX="${ES_ALIAS}_v${NEW_VERSION}"

jq -cn --arg old_index "$OLD_INDEX" --arg new_index "$NEW_INDEX" --arg archive_dir "$ARCHIVE_DIR" \
    '{"level":"info", "message":"Started re-index job", "old_index": $old_index, "new_index": $new_index, "archive_dir": $archive_dir}'

# --[[ CREATE NEW INDEX ]]--
cd "$(dirname "$(dirname "$(realpath "$0")")")"
ES_INDEX=$NEW_INDEX ES_ALIAS=NONE ./scripts/migrate.sh

# --[[ LOAD RECORDS INTO NEW INDEX ]]--
cd "$(dirname "$(dirname "$(realpath "$0")")")/app"
ES_INDEX=$NEW_INDEX ./import_records.pl "${ARCHIVE_DIR}"
info "Loaded records from archive, archive dir: ${ARCHIVE_DIR}"

# --[[ UPDATE INDEX ALIAS ]]--
http --check-status --quiet --body POST "${ES_HOST}/_aliases" << EOF
    {
        "actions" : [
            { "remove" : { "index" : "${OLD_INDEX}", "alias" : "${ES_ALIAS}" } },
            { "add" : { "index" : "${NEW_INDEX}", "alias" : "${ES_ALIAS}" } }
        ]
    }
EOF
info "Updated alias ${ES_ALIAS} from ${OLD_INDEX} to ${NEW_INDEX}"

# --[[ DELETE OLD INDEX ]]--
http --check-status --quiet DELETE "${ES_HOST}/${OLD_INDEX}"
info "Deleted index ${OLD_INDEX}"

jq -cn --arg old_index "$OLD_INDEX" --arg new_index "$NEW_INDEX" --argjson took $SECONDS \
    '{"level": "info", "message":"Completed re-index job", "old_index": $old_index, "new_index": $new_index, "took": $took}'
