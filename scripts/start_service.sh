#!/bin/bash
set -euo pipefail

if [[ -z "$1" ]]; then
  echo "Usage: start_service.sh {sru|oai|rest}"
  die 1
fi
SERVICE=$1

CATMANDU_DIR=${CATMANDU_DIR:-/app}
LISTEN=${LISTEN:-:5000}
RUN_USER=${RUN_USER:-www-data}
RUN_GROUP=${RUN_GROUP:-www-data}
APP_ENV=${APP_ENV:-production}

case "$SERVICE" in
  oai|sru|rest)
    PSGI_FILE=${CATMANDU_DIR}/app/bin/${SERVICE}.psgi
    ;;

  *)
    echo "ERROR: Unknown service: ${SERVICE}"
    die 1
    ;;
esac

echo "Starting service: ${SERVICE}"
echo " :: Environment: ${APP_ENV}"
echo " :: Catmandu dir: ${CATMANDU_DIR}"
echo " :: Perl version: $(perl -e 'print "$^V\n"')"

# TODO: Check connectivity

cd "${CATMANDU_DIR}/app"

exec /usr/local/bin/starman \
  --listen "${LISTEN}" \
  --user "${RUN_USER}" \
  --group "${RUN_GROUP}" \
  --workers 5 \
  --max-requests 1000 \
  -E "${APP_ENV}" \
  -I"${CATMANDU_DIR}/app/lib" \
  -I"${CATMANDU_DIR}/vendor/Dancer-Plugin-Catmandu-SRU/lib" \
  -I"${CATMANDU_DIR}/vendor/Dancer-Plugin-Catmandu-OAI/lib" \
  "${PSGI_FILE}"
