describe('Publications REST API', () => {

  const BASE_URL = 'http://localhost:5000/publications/v1';
  const KNOWN_ISBN = '9788284191027';
  const UNKNOWN_ISBN = '9788284191022';

  it('should return no records for unknown ISBN', () => {
    cy.request({
      method: 'GET',
      url: `${BASE_URL}/?query=${UNKNOWN_ISBN}`,
    })
      .should((response) => {
        expect(response.headers, 'content type').to.include({
          'content-type': 'application/json; charset=utf-8',
        });
        expect(response.body.total).to.equal(0);
      });
  });

  it('should return a single record for known ISBN', () => {
    cy.request({
      method: 'GET',
      url: `${BASE_URL}/?query=${KNOWN_ISBN}`,
    })
      .should((response) => {
        expect(response.headers, 'content type').to.include({
          'content-type': 'application/json; charset=utf-8',
        });
        expect(response.body.total).to.equal(1);
        expect(response.body.records[0].isbn).to.equal(KNOWN_ISBN);
      });
  });

  it('should not include 7XX title entries as creator', () => {
    cy.request({
      method: 'GET',
      url: `${BASE_URL}/0554644`, // 9788202621193
    })
      .should((response) => {
        expect(response.body.creator).to.have.length(1);
      });
  });

});
