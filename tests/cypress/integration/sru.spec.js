describe('SRU API', () => {

  const SRU_BASE_URL = 'http://localhost:8001/mlnb';
  const KNOWN_ISBN = '9788284191027';
  const UNKNOWN_ISBN = '9788284191022';

  it('should return no records for unknown ISBN', () => {
    cy.request({
      method: 'GET',
      url: `${SRU_BASE_URL}?version=1.1&operation=searchRetrieve&maximumRecords=10&recordSchema=marc21&query=${UNKNOWN_ISBN}`,
      headers: {
        accept: 'application/xml',
      },
    })
      .should((response) => {
        expect(response.headers, 'content type').to.include({
          'content-type': 'text/xml; charset=utf-8',
        });
        const $xml = Cypress.$(response.body);
        const $records = $xml.find('records > record > recordData > record');
        expect($records.length).to.equal(0);
      });
  });

  context('MARC 21', () => {

    [
      "marc21",
      "marc21nor"
    ].forEach(recordSchema => {
      it(`${recordSchema}: should return a single record for known ISBN`, () => {
        cy.request({
          method: 'GET',
          url: `${SRU_BASE_URL}?version=1.1&operation=searchRetrieve&maximumRecords=10&recordSchema=${recordSchema}&query=${KNOWN_ISBN}`,
          headers: {
            accept: 'application/xml',
          },
        })
          .should((response) => {
            expect(response.headers, 'content type').to.include({
              'content-type': 'text/xml; charset=utf-8',
            });
            const $xml = Cypress.$(response.body);
            const $records = $xml.find('records > record > recordData > record');
            expect($records.length).to.equal(1);
            const isbn = $records.find('datafield[tag="020"] > subfield[code="a"]').text()
            expect(isbn).to.equal(KNOWN_ISBN);
          });
      });
    })
  });

  context('MODS', () => {
    it('should return a single record for known ISBN', () => {
      cy.request({
        method: 'GET',
        url: `${SRU_BASE_URL}?version=1.1&operation=searchRetrieve&maximumRecords=100&recordSchema=mods&query=${KNOWN_ISBN}`,
        headers: {
          accept: 'application/xml',
        },
      })
        .should((response) => {
          expect(response.headers, 'content type').to.include({
            'content-type': 'text/xml; charset=utf-8',
          });
          const $xml = Cypress.$(response.body);
          const $records = $xml.find('records > record > recordData > mods');
          expect($records.length).to.equal(1);
          const isbn = $records.find('identifier[type="isbn"]').text()
          expect(isbn).to.equal(KNOWN_ISBN);
        });
    });
  });

});
